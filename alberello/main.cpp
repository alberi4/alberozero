#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct node{
    int data;
    struct node* left;
    struct node* right;
};s

struct node* createNode(int value){
    struct node* newNode = (struct node*)malloc(sizeof(struct node));
    newNode->data = value;
    newNode->left = NULL;
    newNode->right = NULL;
    return newNode;
}

struct node* insert(struct node* root, int data)
{
    if (root == NULL) return createNode(data);
    if (data < root->data)
        root->left  = insert(root->left, data);
    else if (data > root->data)
        root->right = insert(root->right, data);
    return root;
}

void inorder(struct node* root){
    if(root == NULL) return;
    inorder(root->left);
    printf("%d ->", root->data);
    inorder(root->right);
}

void preorder(struct node* root){
    if(root == NULL) return;
    printf("%d ->", root->data);
    preorder(root->left);
    preorder(root->right);
}

void postorder(struct node* root){
    if(root == NULL) return;
    postorder(root->left);
    postorder(root->right);
    printf("%d ->", root->data);
}

int search(struct node* root, int number){
if (root == NULL)
    return NULL;
else if (number == root->data)
    return root->data;
else if (number < root->data)
    return search(root->left,number);
else if (number > root->data)
    return search(root->right,number);
else
    return 0;
}

//foglia: se i puntatori di sx e dx non puntano a nulla
void ContaFoglie(struct node* root, int numeroFoglie){
    
    
}
 
//nodo: se punta ad almeno ad un'altro nodo o foglia
void ContaNodi(){
}
 
/*
void MediaFoglie(){
}
int getLevelUtil(struct node *root, int data, int level)
{
    if (root == NULL)
        return 0;
  
    if (root->data == data)
        return level;
  
    int downlevel = getLevelUtil(root->left, data, level+1);
    if (downlevel != 0)
        return downlevel;
  
    downlevel = getLevelUtil(root->right, data, level+1);
    return downlevel;
}
  
int getLevel(struct node *root, int data)
{
    return getLevelUtil(root,data,0);
}
*/

int main(){
    int x,n,prova;
    struct node *root = NULL;
/*
    //inserimento casuale
    srand(time(NULL));
    for(i=0;i<10;i++){
        dato=rand()%100+1;
        if(i==0)
            root= insert(root,dato);
        else
            insert(root, dato);
    }
*/
    root = insert(root, 50);
    insert(root, 40);
    insert(root, 60);
    insert(root, 30);
    insert(root, 44);
    insert(root, 55);
    insert(root, 70);
    insert(root, 20);
    insert(root, 35);
    insert(root, 42);
    insert(root, 46);
    insert(root, 80);
    insert(root, 41);
    insert(root, 43);
    insert(root, 45);
    insert(root, 48);
    insert(root, 90);
    
    while(x>0){
        printf("\n[0]Esci.\n[1]Visualizza\n[2]Cerca\n[3]Conta foglie\n[4]Conta nodi\n");
        scanf("%d",&x);
    switch(x){
        case 1:
            printf("In che modo vuoi visualizzare:\n[1]InOrder\n[2]PostOrder\n[3]PreOrder\n");
            scanf("%d",&x);
            if(x==1)
                inorder(root);
            else if(x==2)
                postorder(root);
            else if(x==3)
                preorder(root);
            break;
        case 2:
            printf("Inserisci numero da cercare: ");
            scanf("%d",&n);
            if(n==search(root, n))
                printf("Numero %d presente.\n",n);
            else
                printf("Numero non presente.\n");
            break;
        case 3:
            break;
        case 4:
            break;
    }
    }
}
